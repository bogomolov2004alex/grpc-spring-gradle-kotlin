# Introduction

This is my first project on spring boot using gRPC, microservices and multi-modules. I tried to work only with unary RPC calls? but excited to learn mote about streams too.

## Get started

First enter the `/interface-project` directory and enter following command:\
`gradle compileKotlin`

Then start the microservice, located in the `/server-project` directory.