import com.google.protobuf.gradle.*

plugins {
    kotlin("jvm")
    id("com.google.protobuf") version "0.8.18"
}

val protobufVersion: String by extra("3.23.4")
val grpcVersion: String by extra("1.58.0")

dependencies {
    implementation("io.grpc:grpc-protobuf:$grpcVersion")
    implementation("io.grpc:grpc-stub:$grpcVersion")
    testImplementation(kotlin("test"))
    compileOnly("jakarta.annotation:jakarta.annotation-api:1.3.5")
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                id("grpc") {  }
            }
        }
    }
}

// Optional
//eclipse {
//    classpath {
//        file.beforeMerged { cp ->
//            def generatedGrpcFolder = new org.gradle.plugins.ide.eclipse.model.SourceFolder('src/generated/main/grpc', null);
//            generatedGrpcFolder.entryAttributes['ignore_optional_problems'] = 'true';
//            cp.entries.add( generatedGrpcFolder );
//            def generatedJavaFolder = new org.gradle.plugins.ide.eclipse.model.SourceFolder('src/generated/main/java', null);
//            generatedJavaFolder.entryAttributes['ignore_optional_problems'] = 'true';
//            cp.entries.add( generatedJavaFolder );
//        }
//    }
//}

// Optional
sourceSets {
    main {
        java {
            srcDirs("build/generated/source/proto/main/grpc")
            srcDirs("build/generated/source/proto/main/java")
        }
    }
}

//kotlin {
//    jvmToolchain(21)
//}