package ru.mirea.service

import io.grpc.stub.StreamObserver
import net.devh.boot.grpc.server.service.GrpcService
import ru.mirea.grpcspringkotlin.lib.HelloReply
import ru.mirea.grpcspringkotlin.lib.HelloRequest
import ru.mirea.grpcspringkotlin.lib.MyServiceGrpc

@GrpcService
class MyServiceImpl : MyServiceGrpc.MyServiceImplBase() {
    override fun sayHello(request: HelloRequest?, responseObserver: StreamObserver<HelloReply>?) {
        val message = request?.name

        if (message != null) {
            println("Received name: $message")
        }

        println("Received request!!!")

        val helloReply = HelloReply.newBuilder()
            .setMessage("Hello $message, from server project")
            .build()

        responseObserver?.apply {
            onNext(helloReply)
            onCompleted()
        }
    }
}