package ru.mirea

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ServerProjectSpringBootApplication

fun main(args: Array<String>) {
    runApplication<ServerProjectSpringBootApplication>(*args)
}
